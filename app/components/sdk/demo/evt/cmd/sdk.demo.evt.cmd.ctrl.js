(function () {

	var app = angular.module('atControllers');

	app.controller('SdkDemoEvtCmdCtrl', ['$rootScope', '$scope', '$sdkXmlSvc', '$sdkDeviceSvc', '$sdkEvtSvc',
		function ($rootScope, $scope, $sdkXmlSvc, $sdkDeviceSvc, $sdkEvtSvc) {
		"use strict";

		var vm = this;
		var evtCmd = [
			{
				label: "Read",
				cmd: "read"
			},
			/*{
				label: "Query with filter",
				cmd: "query"
			},*/
			{
				label: "Query and read with filter",
				cmd: "query_read"
			},
		];

		function initialize() {
			vm.rqst = {
				cmd: "read",
				serial: $sdkXmlSvc.getSetTargetSerial(),
				serial_min: $sdkXmlSvc.getSetTargetSerial(),
				serial_max: $sdkXmlSvc.getSetTargetSerial(),
				objType: 65534,
				objTypeMin: 0,
				objTypeMax: 65534,
				idMin: 0,
				idMax: 0xFFFFFFFE,
				lastTsStr: "-1",
				lastSernum: "FFFFFFFF",
				lastNum: "FFFFFFFF",
				nb_events : 40,
				checkAll : true
			};
			vm.ObjectIdLabel = "Object Id";
			vm.loading = false;
			var SerialAll = {
				serial: "FFFFFFFF",
				label: "ALL SYSTEM"
			};
			$rootScope.$broadcast("SdkDemoEvtCmdCtrl::init");
			vm.serialList = $sdkDeviceSvc.getDeviceListByMaster($sdkXmlSvc.getSetTargetSerial());
			//vm.serialList.push(SerialAll);


			vm.TypeList = [];
			vm.TypeList.push( { label: "ALL", id: 65534, 	objTypeMin: 0,  objTypeMax: 65534});
			vm.TypeList.push( { label: "Door", id: 4,  		objTypeMin: 4,  objTypeMax: 4});
			vm.TypeList.push( { label: "Partition", id: 10, objTypeMin: 10, objTypeMax: 10});
			vm.TypeList.push( { label: "User", id: 11,  	objTypeMin: 11, objTypeMax: 11});
			vm.TypeList.push( { label: "Card", id: 12,  	objTypeMin: 12, objTypeMax: 12});

			$sdkXmlSvc.sdkParserCallbackRegister($sdkDeviceSvc.onSdkItemParse);
		}
		function onSearchUpdate(e, searchText) {
			vm.searchText = searchText;
		}
		function onShowMe(e, show) {
			vm.show = show;
		}
		function onEvtSelected(e, evt){
			vm.rqst.lastTsStr = evt.timeUtc;
			vm.rqst.lastSernum = evt.serial;
			vm.rqst.lastNum = evt.id;
		}
		function retrieveUsedEvt () {
			var rqst = {
				cmd: 'info',
				recTag: 'cfg',
				serial: vm.rqst.serial,
				objType: 'evt_save'
			};

			vm.loading = true;
			$sdkXmlSvc.postRec(rqst, onRetieveUsedEvt, onRetrieveFail);
		}
		function onRetieveUsedEvt (data) {
			vm.loading = false;
			var idMax = $sdkDeviceSvc.getSetObjIdUsed(vm.rqst.serial, 'evt_save');
			if (idMax != 0){
				vm.rqst.idMax = parseInt(idMax);
			}
			vm.idLast = $sdkDeviceSvc.getSetObjIdMax(vm.rqst.serial, 'evt_save');
		}
		function onRetrieveFail (){
			vm.loading = false;
		}

		var close = function(){
			initialize();
		}
		var selectCmd = function (cmdObj) {
			var SerialAll = {
				serial: "FFFFFFFF",
				label: "ALL SYSTEM"
			};

			if (vm.rqst.cmd != cmdObj.cmd){
				vm.rqst.cmd = cmdObj.cmd;
				vm.serialList = $sdkDeviceSvc.getDeviceListByMaster($sdkXmlSvc.getSetTargetSerial());
			
				if (vm.rqst.cmd == "read"){
					//retrieveUsedEvt();
					vm.ObjectIdLabel = "Event ID Start";
					vm.rqst.serial = $sdkXmlSvc.getSetTargetSerial();
					vm.rqst.serial_min = vm.rqst.serial;
					vm.rqst.serial_max = vm.rqst.serial;
				}
				else {
					vm.ObjectIdLabel = "Object Id";
					vm.serialList.push(SerialAll);
					vm.rqst.serial = "FFFFFFFF";
					vm.rqst.serial_min = "00000000";
					vm.rqst.serial_max = "FFFFFFFF";

					vm.rqst.idMin = 0;
					vm.rqst.idMax = 0xFFFFFFFE;//25000;
				}
			}
		}
		var evtCmdLabelGet = function(){
			for (var i=0; i<vm.evtCmd.length; i++){
				if (vm.rqst.cmd === vm.evtCmd[i].cmd){
					return vm.evtCmd[i].label;
				}
			}
			return null;
		}
		var resetEvtSelected = function(){
			vm.rqst.lastTsStr = "-1";
			vm.rqst.lastSernum = "FFFFFFFF";
            vm.rqst.lastNum = "FFFFFFFF";
		}
		var updateLastEvt = function(){
			var evt = $sdkEvtSvc.getLastEvt();
			vm.rqst.lastTsStr = evt.lastTsStr;
			vm.rqst.lastSernum = evt.lastSernum;
			vm.rqst.lastNum = evt.lastNum;
		}
		function onPostResult(data){
			// updateData();
			vm.loading = false;
		}
		var submit = function () {
			vm.loading = true;

			if(vm.rqst.cmd == 'read')
			{
				vm.rqst.idMax = vm.rqst.idMin + vm.rqst.nb_events;
			}

			$sdkXmlSvc.postEvt(vm.rqst, onPostResult, onPostResult);
		}
		var urlGet = function () {
			var url = $sdkXmlSvc.getSetBaseUrl();
			url = url + "sdk.xml";
			return url;
		}
		var disableUpdateToLast = function () {
			var evtList = $sdkEvtSvc.all();
			if (evtList.length > 0){
				return false;
			}
			return true;
		}

		
		var evtTypeLabelGet = function(){
			for (var i=0; i<vm.TypeList.length; i++){
				if (vm.rqst.objType == vm.TypeList[i].id){
					return vm.TypeList[i].label;
				}
			}
			return null;
		}

		var evtTypeClick = function(type){

			vm.rqst.objType = type.id;

			for (var i=0; i<vm.TypeList.length; i++){
				if (vm.rqst.objType == vm.TypeList[i].id){
					vm.rqst.objTypeMin =  vm.TypeList[i].objTypeMin;
					vm.rqst.objTypeMax =  vm.TypeList[i].objTypeMax;
				}
			}
			return null;
		}
		
		var showQuery = function(){
			if(vm.rqst.cmd == "query_read")
			{
				return true;
			}
			if(vm.rqst.cmd == "query")
			{
				return true;
			}
			return false;
		}

		var showObjId = function(){

			if(vm.rqst.objType == 65534)
			{
				return false;
			}
			return true;
		}

		var AllClick = function(){
			if(vm.rqst.checkAll == true)
			{
				vm.rqst.idMin = 0;
				vm.rqst.idMax = 0xFFFFFFFE;
			}	

		}
		// Register events
		$scope.$on("NavSearchCtrl::searchUpdate", onSearchUpdate);
		$scope.$on("SdkDemoEvtMainCtrl::showEvtCmd", onShowMe);
		$scope.$on("SdkDemoEvtMainCtrl::evtSelected", onEvtSelected);

		// Expose public properties
		vm.rqst = {};
		vm.loading = true;
		vm.show = false;
		vm.searchText = "";
		vm.evtCmd = evtCmd;
		vm.serialList = [];
		vm.idLast = "";

		// Expose public methods
		vm.close = close;
		vm.submit = submit;
		vm.selectCmd = selectCmd;
		vm.evtCmdLabelGet = evtCmdLabelGet;
		vm.resetEvtSelected = resetEvtSelected;
		vm.updateLastEvt = updateLastEvt;
		vm.urlGet = urlGet;
		vm.disableUpdateToLast = disableUpdateToLast;
		vm.evtTypeLabelGet = evtTypeLabelGet;
		vm.evtTypeClick = evtTypeClick;
		vm.showQuery = showQuery;
		vm.AllClick = AllClick;
		vm.showObjId = showObjId;
		// Start init
		initialize();
	}]);

})();